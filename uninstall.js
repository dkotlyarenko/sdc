const Service = require('node-windows').Service;

let svc = new Service({
  name:'shutdown service )',
  script: require('path').join(__dirname,'index.js')
});

svc.on('uninstall',function(){
  console.log('Uninstall complete.');
  console.log('The service exists: ',svc.exists);
});

svc.uninstall();