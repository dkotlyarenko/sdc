const sleepMode = require('sleep-mode');
const winTools = require('wintools');

const path = require('path');
const express = require('express');
const app = express();
const server = require('http').createServer(app);
const port = 666;
const bodyParser = require('body-parser');

const SamsungRemote = require('samsung-remote');
const powerOffCode = 'KEY_POWEROFF';
const remote = new SamsungRemote({
    ip: '192.168.1.144',
	tvAppString: "iphone.UE32EH6037.iapp.samsung"
});
const pairedWithTvIp = '192.168.1.101';

let req_host = 'localhost';

const osu = require('node-os-utils');
const os = require('os');

const winAudio = require('win-audio').speaker;

const WebSocket = require('ws');
let wss = new WebSocket.Server({ server });

let totalMem = os.totalmem();
let freeMem;

pairedWithTv = function(){
	return req_host.split(':')[0] == pairedWithTvIp;
};

powerOffTv = function() {
	if (pairedWithTv(req_host)) {
		remote.send(powerOffCode, (err) => {
			if (err) {
				console.log(err);
			} else {
				true
			}
		});
	};
};

app.use(bodyParser());
app.set('view engine', 'pug');
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'assets')));

app.get('/*',function(request, response, next){
	req_host = request.get('host');
	next();
});


app.get('/', (request, response) => {
	freeMem = os.freemem();

	osu.cpu.usage()
		.then(cpuLoad => {
			response.render('index', {
				title: req_host,
				clients: wss.clients.size,
				cpuLoad: cpuLoad.toString(),
				freeMem: 100 - ((freeMem / totalMem) * 100),
				upTime: os.uptime(),
				volume: {
					mute: winAudio.isMuted(),
					level: winAudio.get()
				}
			});
		})
		.catch(err => {
			console.log(err);
		});
});

app.get('/sleep', (request, response) => {
	response.send('Zzzzz...');
	powerOffTv();
	sleepMode(function (err, stderr, stdout) {});
});

app.get('/reboot', (request, response) => {
	response.send('I`m restarting...');
	winTools.shutdown.restart(function(){});
});

app.get('/die', (request, response) => {
	response.send('I`m dying!!!');
	powerOffTv();
	winTools.shutdown.poweroff(function(){});
});

app.get('/rip', (request, response) => {
	response.send('I`m dying!!!');
	powerOffTv();
	winTools.shutdown.poweroff(function(){});
});

app.post('/volume', (request, response) => {
	volume = request.body['volume'];
	if (volume) {
		winAudio.set(parseInt(volume));
		response.send(volume);
	} else {
		winAudio.toggle();
		response.send(winAudio.isMuted());
	}
});

app.get('/load', (request, response) => {
	osu.cpu.usage()
		.then(cpuLoad => {
			freeMem = os.freemem();
			response.send({
				time: new Date().getTime(),
				clients: wss.clients.size,
				cpuLoad: cpuLoad.toString(),
				freeMem: 100 - ((freeMem / totalMem) * 100),
				upTime: os.uptime(),
				volume: {
					mute: winAudio.isMuted(),
					level: winAudio.get()
				}
			});
		})
		.catch(err => {
			console.log(err);
		});
});

wss.broadcast = function broadcast(data) {
	wss.clients.forEach(function each(client) {
		if (client.readyState === WebSocket.OPEN) {
			client.send(data);
		}
	});
};

checkInterval = setInterval(function(){
	if (wss.clients.size > 0){
		freeMem = os.freemem();
		osu.cpu.usage()
			.then(cpuLoad => {
				wss.broadcast(JSON.stringify({
						time: new Date().getTime(),
						clients: wss.clients.size,
						cpuLoad: cpuLoad.toString(),
						freeMem: 100 - ((freeMem / totalMem) * 100),
						upTime: os.uptime(),
						volume: {
							mute: winAudio.isMuted(),
							level: winAudio.get()
						}
					}));
			})
			.catch(err => {
				console.log(err);
			});
	}
}, 1000);

server.listen(port, (err) => {
	if (err) {
		return console.log('something bad happened', err);
	}
	console.log('server is listening on ' + port);
});