class @Alert
  @show = (message, type, allow_dismiss=true)->

    delay = 25000

    if type == 'error' || type == 'warning'
      delay = false

    new Noty({
      type: type
      text: message
      layout: 'topRight'
      timeout: delay
      theme: 'bootstrap-v4'
      closeWith: ['click', 'button']
    }).show()