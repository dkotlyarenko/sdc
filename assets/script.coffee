$ ->
  window.lastWsTime = new Date()

  $('a').click (e)->
    e.preventDefault()

    url = $(@).attr('href')

    $.get(url)
      .done (a)->
        Alert.show(a, 'success')
      .fail (e)->
        Alert.show(e, 'error')
      .always (a)->
        console.log a

  convertMS = (seconds)->
    seconds =  Math.floor seconds
    minute = Math.floor(seconds / 60);
    seconds = seconds % 60;
    hour = Math.floor(minute / 60);
    minute = minute % 60;
    day = Math.floor(hour / 24);
    hour = hour % 24;
    return {
      day: day,
      hour: hour,
      minute: minute,
      seconds: seconds
    }

  sendVolume = (value)->
    $.post('/volume', {volume: value}, (response)->
      $('#volumeRange').val response
    )

  getDataWS = ->
    socket = new WebSocket("ws://#{window.location.href.split('//')[1]}load")

    socket.onmessage = (event)->
      window.lastWsTime = new Date()
      data = JSON.parse(event.data)
      showData(data)
      setTimeout (->
        setTimeout (->
          $('#wsPing').addClass('active')
        ), 500
        $('#wsPing').removeClass('active')
      ), 500

    socket.onclose = ()->
      socket = null
      setTimeout (->
        getDataWS();
      ), 5000

  showData = (data)->
    cpuLoad = parseFloat(data['cpuLoad'])
    freeMem = parseFloat(data['freeMem'])
    upTime = parseFloat(data['upTime'])
    volume = parseFloat(data.volume.level)
    volumeMute = data.volume.mute
    @lineCPU.append(data.time, cpuLoad)
    @lineMEM.append(data.time, freeMem)

    $('#cpuLoad').text ->
      "CPU: #{Math.floor cpuLoad}%"

    $('#freeMem').text ->
      "MEM: #{ Math.floor freeMem}%"

    $('#lastWsTime').text ->
      data.clients

    $('#upTime').text ->
      time = convertMS(upTime)
      return "Uptime: #{time.day}d #{time.hour}h #{time.minute}m #{time.seconds}s"

    $('#volumeRange').val volume

    if volumeMute
      $('#btnUnMute').removeClass 'd-none'
      $('#btnMute').addClass 'd-none'
    else
      $('#btnMute').removeClass 'd-none'
      $('#btnUnMute').addClass 'd-none'

  initGraph = ->
    @smoothie = new SmoothieChart
      responsive: true
      millisPerPixel: 100
      minValue: 0
      maxValue: 100
      timestampFormatter: SmoothieChart.timeFormatter
      grid:
        fillStyle: 'transparent'
        strokeStyle:'rgba(119,119,119,0.5)'
        millisPerLine: 10000
        borderVisible: false
        verticalSections: 20
        sharpLines: true

      labels:
        disabled: true

      horizontalLines:[
        { color:'#00936d', lineWidth:1, value:25 }
        { color:'#908700', lineWidth:1, value:50 }
        { color:'#a5002e', lineWidth:1, value:75 }
      ]

    @smoothie.streamTo(document.getElementById("canvasCPU"), 1000)
    @lineCPU = new TimeSeries()
    @lineMEM = new TimeSeries()

    @smoothie.addTimeSeries(@lineCPU,{
      lineWidth:2
      strokeStyle: 'rgba(0,188,140, 1)'
      fillStyle: 'rgba(0,188,140, 0.2)'
    })
    @smoothie.addTimeSeries(@lineMEM,{
      lineWidth:2
      strokeStyle:'#008bff'
    })

  $('#volumeRange').on 'change', ->
    value = $(@).val()
    sendVolume(value)

  $('#volumeRange').on 'mousewheel', (e)->
    current_value = parseInt $(@).val()
    if e.deltaY > 0
      sendVolume current_value + 2
    else
      sendVolume current_value - 2

  $('#btnMute, #btnUnMute').click (e)->
    e.preventDefault()
    $.post('/volume', {mute: 'toggle'})

  $(document).on 'keydown', (e)->
    key = e.which
    current_value = parseInt $('#volumeRange').val()
    if key == 77
      $.post('/volume', {mute: 'toggle'})
    if key == 38
      sendVolume current_value + 2
    if key == 40
      sendVolume current_value - 2

  initGraph()
  getDataWS()